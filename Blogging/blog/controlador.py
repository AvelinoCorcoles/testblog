# -*- coding: utf-8 -*-
from django.db.models import Avg

from blog.models import Articulos, Ratios


class ControladorRatios():
    def get(self, id):
        return Ratios.objects.get(id=id)

    def all(self):
        return Ratios.objects.all()

    def articulo(self, articulo):
        return self.all().filter(articulo=articulo)

    def comentario(self, comentario):
        return self.all().filter(comentario=comentario)

    def articulo_promedio_nota(self, ratios):
        nota = ratios.aggregate(Avg('nota'))
        media = nota.__getitem__('nota__avg')
        return media
