# -*- coding: utf-8 -*-
import datetime
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _
from ckeditor.fields import RichTextField


# Create your models here.

class Articulos(models.Model):
    titulo = models.CharField(
        _('Título'),
        blank=False,
        null=False,
        max_length=300
    )
    imagen = models.ImageField(
        _('Imagen destacada'),
        blank=True,
        null=True,
        upload_to='articulos/'
    )
    contenido = RichTextField(
        _('Contenido'),
        blank=False,
        null=False
    )
    usuario = models.ForeignKey(
        User,
        blank=False,
        null=False,
        related_name='articulos_usuario',
        verbose_name=_('Usuario creación'),
        on_delete=models.CASCADE
    )
    fechacreacion = models.DateTimeField(
        _('Fecha de creación'),
        auto_now_add=True
    )
    fechapublicacion = models.DateTimeField(
        _('Fecha de publicacíon'),
        blank=True,
        null=True
    )
    nota = models.IntegerField(
        _('Nota media'),
        default=0
    )

    def __str__(self):
        if self.fechapublicacion:
            return _('Artículo %s, creado por %s, en fecha %s, publicado en fecha %s.') % (
                self.titulo, self.usuario.username, self.fechacreacion.strftime('%d/%m/%Y'),
                self.fechapublicacion.strftime('%d/%m/%Y')
            )
        else:
            return _('Artículo %s, creado por %s, en fecha %s') % (
                self.titulo, self.usuario.username, self.fechacreacion.strftime('%d/%m/%Y')
            )

    class Meta:
        verbose_name = _('Atículo')
        verbose_name_plural = _('Articulos')
        permissions = (
            ('view_articulo', _('Puede ver artículos')),
        )


class Comentarios(models.Model):
    articulo = models.ForeignKey(
        Articulos,
        blank=False,
        null=True,
        related_name=_('comentario_articulo'),
        on_delete=models.CASCADE
    )
    titulo = models.CharField(
        _('Título'),
        blank=False,
        null=False,
        max_length=300
    )
    imagen = models.ImageField(
        _('Imagen destacada'),
        blank=True,
        null=True,
        upload_to='articulos/'
    )
    contenido = RichTextField(
        _('Contenido'),
        blank=False,
        null=True
    )
    usuario = models.ForeignKey(
        User,
        blank=False,
        null=False,
        related_name='comentarios_usuario',
        verbose_name=_('Usuario creación'),
        on_delete=models.CASCADE
    )
    fechacreacion = models.DateTimeField(
        _('Fecha de creación'),
        auto_now_add=True,
    )
    comentario = models.ForeignKey(
        'Comentarios',
        blank=True,
        null=True,
        related_name=_('comentario_comentado'),
        on_delete=models.CASCADE
    )
    nota = models.IntegerField(
        _('Nota media'),
        default=0
    )

    def __str__(self):
        if self.comentario:
            return _('RE de comentario %s, fecha %s, usuario %s') % (
                self.comentario, self.fechacreacion.strftime('%d/%m/%Y'), self.usuario)
        else:
            if self.fechacreacion:
                return _('Comentario del articulo %s, creado por %s, en fecha %s.') % (
                    self.articulo.titulo, self.usuario.username, self.fechacreacion.strftime('%d/%m/%Y')
                )
            else:
                return _('Comentario del articulo %s, creado por %s.') % (
                    self.articulo.titulo, self.usuario.username
                )

    class Meta:
        verbose_name = _('Comentario')
        verbose_name_plural = _('Comentarios')
        permissions = (
            ('view_comentario', _('Puede ver comentario')),
        )


class Ratios(models.Model):
    articulo = models.ForeignKey(
        Articulos,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='ratios_articulo',
        verbose_name=_('Articulo')
    )
    comentario = models.ForeignKey(
        Comentarios,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='ratios_comentario',
        verbose_name=_('Comentario')
    )
    usuario = models.ForeignKey(
        User,
        blank=False,
        null=False,
        on_delete='ratios_usuario',
        verbose_name=_('Usuario')
    )
    nota = models.IntegerField(
        _('Nota'),
        blank=False,
        null=False,
        default=0
    )
    fechacreacion = models.DateTimeField(
        _('Fecha de creación'),
        auto_now_add=True,
    )

    def __str__(self):
        if self.comentario:
            cadena = _('Ratio del comentario %s, nota: %s, realizada por el usuario %s.') % (
                self.comentario, self.nota, self.usuario)
        else:
            cadena = _('Ratio del articulo %s, nota: %s, realizada por el usuario %s.') % (
                self.articulo.titulo, self.nota, self.usuario)
        return cadena

    class Meta:
        verbose_name = _('Ratio')
        verbose_name_plural = _('Ratios')
        permissions = (
            ('view_ratio', _('Puede ver ratio')),
        )
