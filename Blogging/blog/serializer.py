# -*- coding: utf-8 -*-
import datetime
from django.contrib.auth.models import User
from rest_framework import serializers
from django.utils.translation import ugettext as _
from blog.models import Comentarios, Articulos, Ratios


class UserSerializer(serializers.ModelSerializer):
    '''
    Se ha utilizado el User de django, los usuairos creados, estan sin asginar permisos especificos para los
    modelos, y no son ni super user ni staff, pero tendrian acceso al api-rest cuando esta pida autenticación
    '''
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('url', 'username', 'first_name', 'last_name', 'email', 'password')


class ComentariosSerializer(serializers.ModelSerializer):
    usuario = serializers.StringRelatedField(many=False)
    nota = serializers.ReadOnlyField()
    comentario_comentado = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='comentarios-detail'
    )
    ratios_comentario = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='ratios-detail'
    )

    class Meta:
        model = Comentarios
        fields = ['url', 'id', 'titulo', 'imagen', 'contenido', 'fechacreacion', 'usuario', 'articulo', 'comentario',
                  'comentario_comentado', 'nota', 'ratios_comentario']


class BlogSerializer(serializers.ModelSerializer):
    fechapublicacion = serializers.DateTimeField(initial=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    usuario = serializers.ReadOnlyField(source='usuario.username')
    id = serializers.ReadOnlyField()
    fechacreacion = serializers.ReadOnlyField()
    nota = serializers.ReadOnlyField()
    comentario_articulo = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='comentarios-detail'
    )
    ratios_articulo = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='ratios-detail'
    )

    class Meta:
        model = Articulos
        fields = ['id', 'titulo', 'imagen', 'contenido', 'fechacreacion', 'fechapublicacion', 'usuario',
                  'comentario_articulo', 'nota', 'ratios_articulo']


class RatiosSerializer(serializers.ModelSerializer):
    usuario = serializers.ReadOnlyField(source='usuario.username')

    class Meta:
        model = Ratios
        fields = ['url', 'articulo', 'comentario', 'usuario', 'nota']
