# -*- coding: utf-8 -*-
import datetime
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from rest_framework import serializers, viewsets, status
from rest_framework import permissions
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework import mixins
from django.utils.translation import ugettext as _
from rest_framework.viewsets import ModelViewSet

from blog.controlador import ControladorRatios
from blog.models import Articulos, Comentarios, Ratios
from blog.serializer import UserSerializer, BlogSerializer, ComentariosSerializer, RatiosSerializer


class UserViewSet(ModelViewSet, mixins.CreateModelMixin):
    '''
    Se fine como una viewset de creacion de usuarios, sin requisito de autenticacion. Los usuarios generados estan list
    '''
    queryset = User.objects.all().order_by('-username')
    serializer_class = UserSerializer
    permission_classes = (permissions.AllowAny,)

    def perform_create(self, serializer):
        '''
        Añadido para que la contraseña se cifre y funcione el login correctamente.
        :param serializer:
        :return:
        '''
        if serializer.is_valid():
            serializer.validated_data['password'] = make_password(serializer.validated_data['password'])
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ModelViewSetArticulos(ModelViewSet, mixins.UpdateModelMixin):
    fechaactual = datetime.datetime.now()
    queryset = Articulos.objects.filter(fechapublicacion__lte=fechaactual).order_by('-fechapublicacion')
    serializer_class = BlogSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_backends = (OrderingFilter, DjangoFilterBackend,)
    ordering_fields = ('titulo', 'fechapublicacion')
    filter_field = ('titulo')
    lookup_field = 'id'

    def delete(self, request, *args, **kwargs):
        usuario = request.user
        pk = kwargs.pop('id')
        object = Articulos.objects.get(id=pk)
        if object.usuario == usuario:
            object.delete()
        else:
            raise serializers.ValidationError(_('Este articulo no te pertenece, por lo que no puedes borrarlo.'))

    def perform_create(self, serializer):
        request = self.request
        usuario = request.user
        if serializer.is_valid():
            serializer.validated_data['usuario'] = usuario
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ModelViewSetComentarios(ModelViewSet):
    queryset = Comentarios.objects.all().order_by('-articulo__fechapublicacion')
    serializer_class = ComentariosSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request, *args, **kwargs):
        queryset = Comentarios.objects.filter(comentario=None).order_by('-articulo__fechapublicacion')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            raise serializers.ValidationError(_('Necesitas estar logeado para realizar esta acción'))

    def update(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            if instance.usuario != user:
                raise serializers.ValidationError(_('No eres el propietario del comentario, no puedes modificarlo'))
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)
        else:
            raise serializers.ValidationError(_('Necesitas estar logeado para realizar esta acción'))

    def perform_update(self, serializer):
        request = self.request
        usuario = request.user
        serializer.validated_data['usuario'] = usuario
        serializer.save()

    def perform_create(self, serializer):
        request = self.request
        usuario = request.user
        if serializer.is_valid():
            serializer.validated_data['usuario'] = usuario
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        usuario = request.user
        pk = kwargs.pop('id')
        object = Comentarios.objects.get(id=pk)
        if object.usuario == usuario:
            object.delete()
        else:
            raise serializers.ValidationError(_('Este comentario no te pertenece, por lo que no puedes borrarlo.'))


class ModelViewSetRatios(ModelViewSet):
    queryset = Ratios.objects.all().order_by()
    serializer_class = RatiosSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def create(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            raise serializers.ValidationError(_('Necesitas estar logeado para realizar esta acción'))

    def perform_create(self, serializer):
        request = self.request
        usuario = request.user
        if serializer.is_valid():
            serializer.validated_data['usuario'] = usuario
            articulo = serializer.validated_data['articulo']
            comentario = serializer.validated_data['comentario']
            if articulo != None and comentario != None:
                raise serializers.ValidationError(_('No se puede elegir articulo y comentario a la vez'))
            elif articulo == None and comentario == None:
                raise serializers.ValidationError(_('Debe elegir un articulo o un comentario'))
            serializer.save()
            if articulo:
                ratios = ControladorRatios().articulo(articulo)
                media = ControladorRatios().articulo_promedio_nota(ratios)
                articulo.nota = media
                articulo.save()
            elif comentario:
                ratios = ControladorRatios().comentario(comentario)
                media = ControladorRatios().articulo_promedio_nota(ratios)
                comentario.nota = media
                comentario.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            if instance.usuario != user:
                raise serializers.ValidationError(_('No eres el propietario del comentario, no puedes modificarlo'))
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)
        else:
            raise serializers.ValidationError(_('Necesitas estar logeado para realizar esta acción'))

    def perform_update(self, serializer):
        request = self.request
        usuario = request.user
        serializer.validated_data['usuario'] = usuario
        articulo = serializer.validated_data['articulo']
        comentario = serializer.validated_data['comentario']
        if articulo != None and comentario != None:
            raise serializers.ValidationError(_('No se puede elegir articulo y comentario a la vez'))
        elif articulo == None and comentario == None:
            raise serializers.ValidationError(_('Debe elegir un articulo o un comentario'))
        serializer.save()
