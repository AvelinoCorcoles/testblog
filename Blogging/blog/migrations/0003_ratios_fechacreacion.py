# Generated by Django 2.0 on 2017-12-16 16:23

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20171216_1709'),
    ]

    operations = [
        migrations.AddField(
            model_name='ratios',
            name='fechacreacion',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now, verbose_name='Fecha de creación'),
            preserve_default=False,
        ),
    ]
