# Generated by Django 2.0 on 2017-12-16 16:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_ratios_fechacreacion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ratios',
            name='articulo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ratios_articulo', to='blog.Articulos', verbose_name='Articulo'),
        ),
    ]
