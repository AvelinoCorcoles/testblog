from django.contrib import admin
from blog.models import Articulos, Comentarios, Ratios

# Register your models here.

admin.site.register(Articulos)
admin.site.register(Comentarios)
admin.site.register(Ratios)
