# -*- coding: utf-8 -*-
"""Blogging URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from blog.views import UserViewSet, ModelViewSetComentarios, ModelViewSetArticulos, ModelViewSetRatios

router = routers.DefaultRouter()

router.register(r'users', UserViewSet)
router.register(r'comentarios', ModelViewSetComentarios)
router.register(r'articulos', ModelViewSetArticulos, base_name='articulos')
router.register(r'ratios', ModelViewSetRatios, base_name='ratios')

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^', include(router.urls)),
]
